import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Share from 'react-native-share';

class DemoShare extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onShare = () => {
    Share.open({
      message: 'Demo',
      url: 'https://github.com/react-native-community/react-native-share',
      title: 'Share with',
    }).catch(err => {
      err && console.log(err);
    });
  };

  onShareFBOnly = () => {
    Share.shareSingle({
      social: Share.Social.FACEBOOK,
      url: 'https://github.com/react-native-community/react-native-share',
    }).catch(err => {
      err && console.log(err);
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.onShare} style={styles.btn}>
          <Text style={{color: 'white'}}>Share</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.onShareFBOnly} style={styles.btn}>
          <Text style={{color: 'white'}}>Share FB Only</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default DemoShare;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    width: 100,
    height: 40,
    backgroundColor: 'green',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 15,
  },
});
