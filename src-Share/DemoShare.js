import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Share,
  TouchableOpacity,
  Alert,
} from 'react-native';

class DemoShare extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onShare = async () => {
    try {
      await Share.share({
        message: 'Message share',
        title: 'React Native Share',
      });
    } catch (error) {}
  };

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.onShare} style={styles.btn}>
          <Text style={{color: 'white'}}>Share</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default DemoShare;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    width: 80,
    height: 40,
    backgroundColor: 'green',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
