/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Main from './src/Main2';
import DemoShare from './src-Share/DemoShareLib';

AppRegistry.registerComponent(appName, () => DemoShare);
