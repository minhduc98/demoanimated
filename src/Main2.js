import React, {Component} from 'react';
import {View, StyleSheet, Animated} from 'react-native';
// import Animated from 'react-native-reanimated';

class Main2 extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.colorDot = ['red', 'orange', 'yellow', 'blue', 'green', 'purple'];
    this.marginTop = [
      new Animated.Value(0),
      new Animated.Value(0),
      new Animated.Value(0),
      new Animated.Value(0),
      new Animated.Value(0),
      new Animated.Value(0),
    ];
    this.scale = new Animated.Value(0.3);
  }

  componentDidMount = () => {
    let arrAni1 = [];
    for (let i = 0; i < this.colorDot.length; i++) {
      arrAni1[i] = Animated.timing(this.marginTop[i], {
        toValue: -10,
        duration: 100,
      });
    }
    let arrAni2 = [];
    for (let i = 0; i < this.colorDot.length; i++) {
      arrAni2[i] = Animated.timing(this.marginTop[i], {
        toValue: 0,
        duration: 100,
      });
    }
    let loopAnim1 = Animated.sequence([
      arrAni1[0],
      arrAni2[0],
      arrAni1[1],
      arrAni2[1],
      arrAni1[2],
      arrAni2[2],
      arrAni1[3],
      arrAni2[3],
      arrAni1[4],
      arrAni2[4],
      arrAni1[5],
      arrAni2[5],
    ]);
    let loopAnim2 = Animated.spring(this.scale, {
      toValue: 1,
      friction: 1,
    });
    let loop = Animated.parallel([loopAnim1, loopAnim2]);
    Animated.loop(loop).start();
  };

  render() {
    return (
      <View style={styles.container}>
        <Animated.Image
          source={require('./image/reactnative.png')}
          style={[styles.img, {transform: [{scale: this.scale}]}]}
        />
        <View style={styles.lineDot}>
          {this.colorDot.map((item, index) => (
            <Animated.View
              style={[
                styles.circle,
                {backgroundColor: item, marginTop: this.marginTop[index]},
              ]}
              key={index}
            />
          ))}
        </View>
      </View>
    );
  }
}

export default Main2;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  contentContainerStyle: {
    alignItems: 'center',
  },
  circle: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginLeft: 10,
  },
  img: {
    width: 80,
    height: 80,
    marginBottom: 30,
  },
  lineDot: {
    flexDirection: 'row',
  },
});
