import React, {Component} from 'react';
import {Dimensions, StyleSheet, Animated} from 'react-native';
import {Svg, Circle} from 'react-native-svg';

const {width} = Dimensions.get('window');
const size = width - 32;
const strokeWidth = 50;
const radius = (size - strokeWidth) / 2;
const circumference = 2 * radius * Math.PI;
const CircleAnim = Animated.createAnimatedComponent(Circle);

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      a: new Animated.Value(0),
    };
  }

  componentDidMount = () => {
    Animated.timing(this.state.a, {
      toValue: Math.PI * 2,
      duration: 10000,
    }).start();
  };

  render() {
    const strokeDashoffset = Animated.multiply(this.state.a, radius);
    return (
      <Svg style={styles.container}>
        <Circle
          stroke="#b0b0b0"
          fill="none"
          cy={size / 2}
          cx={size / 2}
          r={radius}
          {...{strokeWidth}}
        />
        <CircleAnim
          stroke="red"
          fill="none"
          cy={size / 2}
          cx={size / 2}
          r={radius}
          strokeDasharray={`${circumference} ${circumference}`}
          {...{strokeWidth, strokeDashoffset}}
        />
      </Svg>
    );
  }
}

export default Main;

const styles = StyleSheet.create({
  container: {
    width: size,
    height: size,
    flex: 1,
    alignSelf: 'center',
  },
});
